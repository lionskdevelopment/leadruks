<?php

namespace App\Http\Controllers\Lead;

use App\Form;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Lead;
use App\Page;

class LeadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $form_id = $request->input(session()->getId());
        $form = Form::findOrFail($form_id);

        //
        $lead = new Lead;
        $lead->form_id = $form_id;
        $lead->session_id = session()->getId();
        $lead->ip_address = $request->ip();
        $lead->user_agent = $request->header('User-Agent');
        $lead->session = collect($request->session())->toJson();
        $lead->header = collect($request->headers)->toJson();
        $lead->server = collect($request->server)->toJson();
        $lead->payload = collect($request)->toJson();
        $lead->completed = true;
        $lead->save();

        // if lead is is submitted... regenerated session
        if ($lead->id) {
            $request->session()->regenerate();
        }

        return view($form->thank_you_template, [
            'url' => url($form->page->root_path)."/",
            'page' => $form->page,
            'parent_page' => $form->page->parent_page_id ? Page::find($form->page->parent_page_id) : null,
            'dids' => $form->page->dids,
            'forms' => $form->page->forms,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
