<?php

namespace App\Http\Controllers\LandingPage;

use App\Page;
use App\AdwordsTracking;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LandingPageController extends Controller
{
    protected $request;

    public function __construct(Request $request) {
        $this->request = $request;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $page = Page::findOrFail($id);

        if($page) {
          $this->adwords_tracking($id, $this->request);
        }
        return view($page->template, [
            'url' => url($page->root_path)."/",
            'page' => $page,
            'parent_page' => $page->parent_page_id ? Page::find($page->parent_page_id) : null,
            'dids' => $page->dids,
            'forms' => $page->forms,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function adwords_tracking($page_id, $request)
    {
        $adwords_tracking = new AdwordsTracking;
        $adwords_tracking->page_id = $page_id;
        $adwords_tracking->ip_address = $request->ip();
        $adwords_tracking->user_agent = $request->header('User-Agent');
        $adwords_tracking->session = collect($request->session())->toJson();
        $adwords_tracking->header = collect($request->headers)->toJson();
        $adwords_tracking->server = collect($request->server)->toJson();
        $adwords_tracking->payload = collect($request)->toJson();
        $adwords_tracking->save();
    }
}
