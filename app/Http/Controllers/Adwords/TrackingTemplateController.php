<?php

namespace App\Http\Controllers\Adwords;

use App\AdwordsTracking;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TrackingTemplateController extends Controller
{
    //
    public function store(Request $request) {

        $adwords_tracking_template = new AdwordsTracking;
        $adwords_tracking_template->ip_address = $request->ip();
        $adwords_tracking_template->user_agent = $request->header('User-Agent');
        $adwords_tracking_template->session = collect($request->session())->toJson();
        $adwords_tracking_template->header = collect($request->headers)->toJson();
        $adwords_tracking_template->server = collect($request->server)->toJson();
        $adwords_tracking_template->payload = collect($request)->toJson();
        $adwords_tracking_template->save();

        return response(200);

    }
}
