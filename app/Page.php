<?php

namespace App;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    use Uuids;
    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    //
    public function domain() {
        return $this->belongsTo('\App\Domain');
    }

    public function forms() {
        return $this->hasMany('\App\Form');
    }

    public function dids() {
        return $this->hasMany('\App\Did');
    }
}
