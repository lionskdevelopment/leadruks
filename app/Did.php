<?php

namespace App;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;

class Did extends Model
{
    use Uuids;
    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    public function campaign()
    {
        return $this->belongsTo('\App\Campaign');
    }

    public function pages()
    {
        return $this->belongsToMany('\App\Page');
    }
}
