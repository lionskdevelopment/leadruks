let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
    .js('resources/assets/js/site.js', 'public/js')
    .js(
        [
            'resources/assets/js/jquery.BSPhotoGallery/jquery.bsPhotoGallery.js',
            'node_modules/wowjs/dist/wow.js',
            'node_modules/bxslider/dist/jquery.bxslider.js',
            'node_modules/google-analytics-js/gajs.js'
        ],
        'public/js/vendor.js'
    )
    .copy('node_modules/bxslider/dist/images/bx_loader.gif', 'public/css/images/bx_loader.gif')

   .sass('resources/assets/sass/app.scss', 'public/css')
    .copy('node_modules/bxslider/dist/jquery.bxslider.css', 'public/css/jquery.bxslider.css')
    .copy('node_modules/animate.css/animate.css', 'public/css/animate.css')
    .styles('resources/assets/css/jquery.bsPhotoGallery.css', 'public/css/jquery/jquery.bsPhotoGallery.css')

    .combine([
        'public/css/app.css',
        'resources/assets/css/jquery.bsPhotoGallery.css'
    ], 'public/css/app.css')

    .copy('resources/assets/images', 'public/images')
;

