@extends('casualty-pro-restoration.landing-page-1-layout')

@section('content')
    <div class="container-fluid header-form-bgimage-1 bgimage-property parallax header-section-space-form-1" id="home">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h1><span class="color-blue">Submit the form now!</span>  Schedule a free water damage inspection</h1>

                <h2 class="mb-20"><span class="color-red">Casualty Pro Restoration</span> is South Florida's trusted mold
                    removal and water damage repair company of choice.</h2>

                <h2 class="mb-20">Over <span class="color-yellow">20 years</span> of experience and the latest remediation
                    equipment, our mold removal and water damage technicians are <span class="color-yellow">licensed</span>,
                    <span class="color-yellow">certified</span> and <span class="color-yellow">bonded.</span>
                    Available 24/7 Open 365 days.</h2>

                <h3 class="mb-5">With direct insurance billing <span class="color-red">PAY ZERO ($O)*</span> out of
                    pocket!</h3>

                <ul class="company_checklist">
                    <li>Water Damage Repair</li>
                    <li>Flood Restoration</li>
                    <li>Mold Removal</li>
                </ul>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 wow fadeInRight">
                <div class="hero-form float-center t-md-left">
                    <div class="hero-form-header text-center">
                        <h2 class="mt-0 mb-0"><span class="color-yellow">FREE</span> SAME DAY INSPECTION</h2>
                        <p class="mb-0 color-white">Immediately available after form submission.</p>
                    </div>
                    <div class="hero-form-wrapper">
                        <form id="lead-form" action="{{ url($forms['0']->submit) }}" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="{{ session()->getId() }}" value="{{ $forms['0']->id }}">

                            <div class="mb-15">
                                <div class="field">
                                    <input type="text" class="form-control input-lg" placeholder="Full Name" name="full_name" required="true">
                                    <i class="fa fa-user font-15"></i>
                                </div>
                            </div>
                            <div class="mb-15">
                                <div class="field">
                                    <input type="text" class="form-control input-lg" placeholder="Phone" name="telephone" required="true">
                                    <i class="fa fa-phone-square font-15"></i>
                                </div>
                            </div>
                            <div class="mb-15">
                                <div class="field">
                                    <input type="email" class="form-control input-lg" placeholder="Email Address" name="email_address">
                                    <i class="fa fa-envelope font-15"></i>
                                </div>
                            </div>
                            <div class="mb-15">
                                <div class="field">
                                    <textarea class="form-control input-lg" placeholder="Details of Damage" name="damage_details"></textarea>
                                </div>
                            </div>
                            <div class="mb-15">
                                <button type="submit" class="btn btn-lg btn-block">I WANT A FREE INSPECTION!</button>
                            </div>
                            <div>
                                <span><i class="fa fa-info-circle font-16 mr-5"></i> We would never share your information.</span>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <h3 class="mb-5"><span class="color-white">Water damage and flood restoration gallery</span></h3>
        <div class="gallery_container">
            <ul class="gallery_restoration">
                <li>
                    <img alt="Casualty Pro Restoration water damage fleet ready 24/7 - 365" src="{{ url('/images/gallery/water-damage-casualty-pro.jpg') }}">
                    <div class="text">Casualty Pro Restoration water damage fleet ready 24/7 - 365</div>
                </li>
                <li>
                    <img alt="Latest water damage and remediation equipment" src="{{ url('/images/gallery/water-damage-detection.jpg') }}">
                    <div class="text">Latest water damage and remediation equipment.</div>
                </li>
                <li>
                    <img alt="Free water damage and mold detection" src="{{ url('/images/gallery/water-damage-latest-instruments.jpg') }}">
                    <div class="text">Free water damage and mold detection</div>
                </li>
                <li>
                    <img alt="Water damage customer in South Miami" src="{{ url('/images/gallery/water-damage-miami.jpg') }}">
                    <div class="text">Water damage customer in South Miami</div>
                </li>
                <li>
                    <img alt="Flood damage after Hurricane Irma" src="{{ url('/images/gallery/water-damage-repair.jpg') }}">
                    <div class="text">Flood damage after Hurricane Irma</div>
                </li>
                <li>
                    <img alt="Pay zero ($0)* out of pocket with our direct insurance billing" src="{{ url('/images/gallery/water-damage-restoration.jpg') }}">
                    <div class="text">Pay zero ($0)* out of pocket with our direct insurance billing</div>
                </li>
            </ul>
        </div>

        <div class="testimonial_div hidden-xs">
            <div class="row">
                <div class="col-md-7">
                    <h5 class="mb-5">Client Testimonials</h5>
                </div>
                <div class="col-md-5">
                    <div id="bx-pager">
                        <ul>
                            <li><a data-slide-index="0" href="">1</a></li>
                            <li><a data-slide-index="1" href="">2</a></li>
                            <li><a data-slide-index="2" href="">3</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <ul class="testimonial">
                <li>
                    <div class="words">"Our roof was damaged in several parts during Hurricane Irma. A friend recommended
                        the guys at Flood Restoration South Florida. I was impressed by their speedy response, professional
                        equipment and personnel. They repaired the water damage and billed the insurance company directly."</div>
                    <div class="person">
                        <div class="img"><img src="{{ url('/images/water-damage-victim-juandiel-martinez.jpg') }}" alt="Water Damage Victims Juandiel & Sara Martinez"/></div>
                        Sarah & Juandiel Martinez, <span class="color-yellow">Homestead, FL</span>
                    </div>
                </li>
                <li>
                    <div class="words">"Overall, their service was very professional. We called several companies and
                        Flood Restoration South Florida was the first to respond. They were at the house working within
                        a few hours, took care of the leak, all the water damage and mold that was building up"</div>
                    <div class="person">
                        <div class="img"><img src="{{ url('/images/water-damage-victim-silvio-anderson.jpg') }}" alt="Water Damage Victims Silvio Anderson"/></div>
                        Diane & Silvio Anderson, <span class="color-yellow">Naples, FL</span>
                    </div>
                </li>
                <li>
                    <div class="words">"We are so grateful for Flood Restoration South Florida! They responded in less
                        than 1 hour and showed up in the same day. Hope we never have water damage again, but if we
                        do... we are certainly calling Flood Restoration South Florida!"</div>

                    <div class="person">
                        <div class="img"><img src="{{ url('/images/water-damage-victim-mariana-salvador-salaga.jpg') }}" alt="Water Damage Victims Mariana & Salvador Sagala"/></div>
                        Mariana & Salvador Sagala, <span class="color-yellow">Homestead, FL</span>
                    </div>
                </li>
            </ul>
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 wow fadeInUp">
            <div class="social">
                <a href="#"><i id="facebook" class="fa fa-facebook" aria-hidden="true"></i></a>
                <a href="#"><i id="google" class="fa fa-google-plus" aria-hidden="true"></i></a>
                <a href="#"><i id="twitter" class="fa fa-twitter" aria-hidden="true"></i></a>
            </div>
        </div>

        <div class="footer">
            <div class="container">
                © <?php echo date('Y'); ?> Flood Restoration Miami / Casualty Pro Restoration, LLC. All Rights Reserved. Powered by Leadruks ®
            </div>
        </div>
</div>
@endsection