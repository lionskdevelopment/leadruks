<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Water Damage Repair, Flood Restoration,  Mold Remediation.</title>
    <meta name="description" content="South Florida's trusted mold & water damage repair company. Water Damage Repair. Flood Restoration. Mold Remediation. Your satisfaction is guaranteed!" />

@if ($page && $page->domain && $page->domain->google_tag_manager_id)
    <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','{{ $page->domain->google_tag_manager_id }}');
        </script>
        <!-- End Google Tag Manager -->
@endif
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="Water Damage Repair, Flood Restoration,  Mold Remediation.">
    <meta itemprop="description" content="South Florida most proven and trusted water damage company. Water Damage Repair, Flood Restoration,  Mold Remediation. Your satisfaction is guaranteed!">

    <link href="{{ url('/css/app.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ url('/css/jquery.bxslider.css') }}" rel="stylesheet" type="text/css" />
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href="{{ url('/css/animate.css') }}" rel="stylesheet" type="text/css" />

</head>

<body>
@if ($page && $page->domain && $page->domain->google_tag_manager_id)
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id={{ $page->domain->google_tag_manager_id }}"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
@endif

<section id="top_header">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 site_logo">
                <a href="{{ url($page->root_path) }}">
                    <img src="{{ url('/images/flood-restoration-south-florida-logo.png') }}" alt="Flood Restoration South Florida">
                </a>
            </div>
            <div class="col-md-6 call-number">
                <div class="call-us">
                    <i class="fa fa-phone" aria-hidden="true"></i>
                    <span>Call</span>
                    <h2><a href="{{ $url .'call' }}">{{ !$parent_page ? $page->dids['0']->number : $parent_page->dids['0']->number }}</a></h2>
                </div>
            </div>
        </div>
    </div>
</section>

<div id="container">
    @yield('content')
</div>

<script src="{{ url('/js/app.js') }}"></script>
<script src="{{ url('/js/vendor.js') }}"></script>
<script src="{{ url('/js/site.js') }}"></script>

</body>

</html>
