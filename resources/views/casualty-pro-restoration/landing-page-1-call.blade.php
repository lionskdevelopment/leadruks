@extends('casualty-pro-restoration.landing-page-1-layout')

@section('content')

    <div class="container-fluid header-form-bgimage-1 bgimage-property parallax header-section-space-form-1" id="home">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Thank you, for your phone call.</h1>
                    <h2>A <span class="color-red">Casualty Pro Restoration</span> specialist will shortly assist you with any Water Damage Repair, Flood Restoration or Mold Removal questions you may have.</h2>

                    <div class="testimonial_div hidden-xs">
                        <h5 class="mb-5">Real Client Testimonials</h5>
                        <ul class="testimonial">
                            <li>
                                <div class="words">"Our roof was damaged in several parts during Hurricane Irma. A friend recommended the guys at Flood Restoration South Florida. I was impressed by their speedy response, professional equipment and personnel. They repaired the water damage and billed the insurance company directly."</div>
                                <div class="person">
                                    <div class="img"><img src="/images/water-damage-victim-juandiel-martinez.jpg" alt="Water Damage Victims Juandiel & Sara Martinez"/></div>
                                    Sarah & Juandiel Martinez, <span class="color-yellow">Homestead, FL</span>
                                </div>
                            </li>
                            <li>
                                <div class="words">"Overall, their service was very professional. We called several companies and Flood Restoration South Florida was the first to respond. They were at the house working within a few hours, took care of the leak, all the water damage and mold that was building up"</div>
                                <div class="person">
                                    <div class="img"><img src="/images/water-damage-victim-silvio-anderson.jpg" alt="Water Damage Victims Silvio Anderson"/></div>
                                    Diane & Silvio Anderson, <span class="color-yellow">Naples, FL</span>
                                </div>
                            </li>
                            <li>
                                <div class="words">"We are so grateful for Flood Restoration South Florida! They responded in less than 1 hour and showed up in the same day. Hope we never have water damage again, but if we do... we are certainly calling Flood Restoration South Florida!"</div>
                                <div class="person">
                                    <div class="img"><img src="/images/water-damage-victim-mariana-salvador-salaga.jpg"/></div>
                                    Mariana & Salvador Sagala, <span class="color-yellow">Homestead, FL</span>
                                </div>
                            </li>
                        </ul>
                    </div>

                </div>

            </div>

        </div>
    </div>


    <script type="text/javascript">
        window.location = "tel://{{ !$parent_page ? $page->dids['0']->number : $parent_page->dids['0']->number }}";
    </script>
@endsection