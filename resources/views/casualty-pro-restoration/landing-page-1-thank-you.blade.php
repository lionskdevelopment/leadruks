@extends('casualty-pro-restoration.landing-page-1-layout')

@section('content')

    <!-- Event snippet for Lead Submitted conversion page -->
    <script>
        gtag('event', 'conversion', {'send_to': 'AW-830788619/bYTNCOuG9ncQi6iTjAM'});
    </script>
    
    <div class="container-fluid header-form-bgimage-1 bgimage-property parallax header-section-space-form-1" id="home">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Thank you for submitting your information.</h1>
                    <h2>A Casualty Pro Restoration mold and water damage specialist will contact you shortly!</h2>
                    <h3 class="mb-20">If you need immediate assistance, call us now at <a href="{{ $url . 'call' }}">{{ !$parent_page ? $page->dids['0']->number : $parent_page->dids['0']->number }}</a></h3>

                    <div class="testimonial_div hidden-xs">
                        <h5 class="mb-5">Real Client Testimonials</h5>
                        <ul class="testimonial">
                            <li>
                                <div class="words">"Our roof was damaged in several parts during Hurricane Irma. A friend recommended the guys at Flood Restoration South Florida. I was impressed by their speedy response, professional equipment and personnel. They repaired the water damage and billed the insurance company directly."</div>
                                <div class="person">
                                    <div class="img"><img src="{{ url('/images/water-damage-victim-juandiel-martinez.jpg') }}" alt="Water Damage Victims Juandiel & Sara Martinez"/></div>
                                    Sarah & Juandiel Martinez, <span class="color-yellow">Homestead, FL</span>
                                </div>
                            </li>
                            <li>
                                <div class="words">"Overall, their service was very professional. We called several companies and Flood Restoration South Florida was the first to respond. They were at the house working within a few hours, took care of the leak, all the water damage and mold that was building up"</div>
                                <div class="person">
                                    <div class="img"><img src="{{ url('/images/water-damage-victim-silvio-anderson.jpg') }}" alt="Water Damage Victims Silvio Anderson"/></div>
                                    Diane & Silvio Anderson, <span class="color-yellow">Naples, FL</span>
                                </div>
                            </li>
                            <li>
                                <div class="words">"We are so grateful for Flood Restoration South Florida! They responded in less than 1 hour and showed up in the same day. Hope we never have water damage again, but if we do... we are certainly calling Flood Restoration South Florida!"</div>
                                <div class="person">
                                    <div class="img"><img src="{{ url('/images/water-damage-victim-mariana-salvador-salaga.jpg') }}"/></div>
                                    Mariana & Salvador Sagala, <span class="color-yellow">Homestead, FL</span>
                                </div>
                            </li>
                        </ul>
                    </div>

                </div>

            </div>
        </div>
    </div>
@endsection