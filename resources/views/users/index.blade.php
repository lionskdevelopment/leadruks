@extends('layouts.app')
@section('title', 'Users Listing')

@section('content')

    @foreach ($users as $user)
        <p>This is user {{ $user->id }} - name: {{ $user->name }}</p>
    @endforeach

@endsection