$(document).ready(function(){
    $('.testimonial').bxSlider({
        controls: false,
        auto: true,
        pagerCustom: '#bx-pager'
    });
    $('ul.gallery_restoration').bsPhotoGallery({
        "classes" : "col-lg-2 col-md-2 col-sm-4 col-xs-12 col-xxs-12",
        "hasModal" : true,
        // "fullHeight" : false
    });
    // $('.social i').hide();
});