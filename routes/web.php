<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::domain('leadruks.net')->group( function() {
    Route::get('/adwords_tracking_template', 'Adwords\TrackingTemplateController@store');
});

$domain_name = get_domain(Request::getHost());
//$doma

$domain = App\Domain::where('name', $domain_name)->first();
//$domain = '';

if ( $domain && $domain->count() >= 1 ) {

    Route::domain($domain->name)->group( function() use($domain) {

        Route::get('/track', 'Adwords\TrackingTemplateController@store');

        $path = "/".Request::path();
        $page = App\Page::where('domain_id', $domain->id)
            ->where('path', $path)->first();

        if ( $page && $page->count() >= 1 ) {

            Route::prefix($path)->group( function() use( $domain, $path, $page ) {

                switch($page->method) {
                    case 'get':

                        Route::get('/', function() use($page) {
                            $app = app();
                            $controller = $app->make('App\Http\Controllers\\'. $page->controller);
                            return $controller->callAction($page->action, $parameters = array('id' => $page->id));
                        });
                        break;
                    case 'post':
                        Route::post('/', 'Lead\LeadController@store');
                        break;
                    default:
                        return view('default-landing-page');
                }
            });

        } else {

            $page = $domain->pages->where('domain_default', '1')->first();

            if ($page) {
                Route::get($page->path, function() use($page) {
                    $app = app();
                    $controller = $app->make('App\Http\Controllers\\'. $page->controller);
                    return $controller->callAction($page->action, $parameters = array('id' => $page->id));
                });
            }

        }

    });


    // fix for www.domain.com
    $page = $domain->pages->where('domain_default', '1')->first();
    Route::get($page->path, function() use($page) {
        $app = app();
        $controller = $app->make('App\Http\Controllers\\'. $page->controller);
        return $controller->callAction($page->action, $parameters = array('id' => $page->id));
    });

    Route::fallback(function() {
        return redirect( '/');
    });

} else {
    dd('here');
    Route::get('/', function () {
        return view('default-landing-page');
    });
}

function get_domain($url)
{
//    $pieces = parse_url($url);
//    $domain = isset($pieces['host']) ? $pieces['host'] : '';
    if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $url, $regs)) {
        return $regs['domain'];
    }
    return false;
}
