# TODO
## Landing Page
- Track the clicks on phone number
- Add photos gallery under "we can help" list
- At the bottom of the form add icons for: facebook, twitter, instagram, bbr (no links)
- Enable WoW animation
- Start to submit form as soon as user changes fields. (to track abandon leads)
- Convert all links and src to laravel url()
- Separate form into a section
- Fix color of phone number as its now a link
- Make sure robots.txt is working
- Set cache
- Utilize CDN
- Enable Localization
- Cache CSS and Javascript with timestamp
- The Call Text should be bigger
- Use CSS font instead of h2 in text that are over short than 15 and larger than 65 characters (for SEO)
- Update routes on front end to be more dynamic and group by domain

## Backend
- Admin dashboard
- Set uuid
- Track siteLinks