<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forms', function (Blueprint $table) {
            $table->uuid('id');
            $table->char('page_id',36);
            $table->tinyInteger('placement')->default(1);
            $table->string('name');
            $table->enum('type', ['Landing Page', 'Form']);
            $table->text('description')->nullable();
            $table->string('submit')->nullable();
            $table->string('thank_you_template');
            $table->text('payload');

            $table->timestamps();
            $table->softDeletes();

            $table->primary('id');
            $table->index('name');
            $table->index('type');

            $table->foreign('page_id')
                ->references('id')->on('pages')
                ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('forms');
    }
}
