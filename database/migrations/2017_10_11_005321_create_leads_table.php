<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leads', function (Blueprint $table) {
            $table->uuid('id');
            $table->char('form_id',36);
            $table->string('session_id');
            $table->string('status')->default('submitted');
            $table->ipAddress('ip_address');
            $table->string('user_agent');
            $table->text('header');
            $table->text('server');
            $table->text('session');
            $table->text('payload');
            $table->boolean('completed');

            $table->timestamps();
            $table->softDeletes();

            $table->primary('id');
            $table->index('status');

            $table->foreign('form_id')
                ->references('id')->on('forms')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leads');
    }
}
