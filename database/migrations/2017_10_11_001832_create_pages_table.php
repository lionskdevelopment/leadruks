<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->uuid('id');
            $table->char('domain_id',36);
            $table->char('parent_page_id',36)->nullable();
            $table->string('name');
            $table->enum('method', ['get', 'post', 'put', 'delete']);
            $table->string('controller')->default('LandingPage\LandingPageController');
            $table->string('action')->default('show');
            $table->string('root_path');
            $table->string('path');
            $table->enum('type', ['Web Site', 'Landing', 'Blog', 'Submit', 'Call']);
            $table->string('template');
            $table->text('payload')->nullable();
            $table->boolean('domain_default')->default('0');

            $table->timestamps();
            $table->softDeletes();

            $table->primary('id');
            $table->index('type');

            $table->foreign('domain_id')
                ->references('id')->on('domains')
                ->onDelete('cascade');
            $table->foreign('parent_page_id')
                ->references('id')->on('pages')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
