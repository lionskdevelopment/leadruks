<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdwordsTrackingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adwords_trackings', function (Blueprint $table) {
            $table->uuid('id');
            $table->char('page_id',36);
            $table->ipAddress('ip_address');
            $table->string('user_agent');
            $table->text('header');
            $table->text('server');
            $table->text('session');
            $table->text('payload');

            $table->timestamps();
            $table->softDeletes();

            $table->primary('id');

            $table->foreign('page_id')
                ->references('id')->on('pages')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adwords_trackings');
    }
}
