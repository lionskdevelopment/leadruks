<?php

use Faker\Generator as Faker;

$factory->define(App\Client::class, function (Faker $faker) {
    return [
        'name' => $faker->company,
        'address_1' => $faker->streetAddress,
        'address_2' => $faker->secondaryAddress,
        'state' => $faker->stateAbbr,
        'city' => $faker->city,
        'zip' => $faker->postcode,
        'phone' => $faker->phoneNumber,
        'email' => $faker->unique()->safeEmail,
        'website' => $faker->domainName,
        'contact_name' => $faker->name,
        'contact_phone' => $faker->phoneNumber,
        'contact_email' => $faker->unique()->safeEmail,
    ];
});
