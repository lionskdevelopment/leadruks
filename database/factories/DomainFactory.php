<?php

use Faker\Generator as Faker;

//$factory->define(App\Domain::class, function (Faker $faker) {
//    return [

//    ];
//});

$factory->define(App\Domain::class, function (Faker $faker) {
    return [
        'client_id' => function () {
            return App\Client::all()->random(1)->first()->id;
        },
        'name' => $faker->unique()->domainName,
    ];
});
