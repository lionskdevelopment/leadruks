<?php

use Faker\Generator as Faker;

$factory->define(App\Form::class, function (Faker $faker) {
    $payload = '{
        "form_type": "'. $faker->randomElement(['Bootstrap', 'Material']) .'",
        "auto_submit": "'. $faker->boolean(50) .'",
        "submit_color": "'. $faker->hexcolor .'",
    }';

    return [
        'page_id' => function () {
            return App\Page::all()->random(1)->first()->id;
        },
        'placement' => $faker->randomElement(['1', '2']),
        'name' => $faker->catchPhrase,
        'type' => $faker->randomElement(['Landing Page', 'Form']),
        'description' => $faker->paragraph,
        'thank_you_template' => 'casualty-pro-restoration/landing-page-1-thank-you',
        'payload' => $payload
    ];
});
