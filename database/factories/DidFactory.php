<?php

use Faker\Generator as Faker;

$factory->define(App\Did::class, function (Faker $faker) {
    return [
        'page_id' => function () {
            return App\Page::all()->random(1)->first()->id;
        },
        'placement' => $faker->randomElement(['1', '2']),
        'number' => $faker->phoneNumber
    ];
});
