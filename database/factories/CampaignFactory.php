<?php

use Faker\Generator as Faker;

$factory->define(App\Campaign::class, function (Faker $faker) {
    return [
        'client_id' => function () {
            return App\Client::all()->random(1)->first()->id;
        },
        'name' => $faker->catchPhrase,
        'start' => $faker->dateTimeThisMonth('now'),
        'stop' => $faker->dateTimeThisMonth('2017-10-23 13:46:23'),
        'description' => $faker->paragraph
    ];
});
