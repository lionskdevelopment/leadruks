<?php

use Faker\Generator as Faker;

$factory->define(App\Lead::class, function (Faker $faker) {
    $json = '{
        "name": "'. $faker->name .'",
        "email": "'. $faker->email .'",
        "phone": "'. $faker->phoneNumber .'",
    }';

    return [
        'form_id' => function () {
            return App\Form::all()->random(1)->first()->id;
        },
        'session_id' => $faker->randomElement([
            'NHCyUUyYSGyYvt8iGngXq1Dq5lVpe4TH5NG0qb7O',
            'ASDFUUyYSGyYvt8iGngXq1Dq5lVpe4TH5NG02344',
            'UEPP4844SGyYvt8iGngXq1Dq5lVpe4DHFDG00934',
            'ASDFUUyYSGyYvt8iGngXq1Dq5lVpe4TH5NG02344',
        ]),
        'status' => $faker->randomElement(['Submitted', 'Viewed', 'Closed']),
        'ip_address' => $faker->ipv4,
        'user_agent' => $faker->randomElement(['Firefox', 'Chrome', 'IE']),
        'header' => $json,
        'server' => $json,
        'session' => $json,
        'payload' => $json,
        'completed' => 1
    ];
});
