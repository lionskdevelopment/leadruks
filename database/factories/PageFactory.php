<?php

use Faker\Generator as Faker;

$factory->define(App\Page::class, function (Faker $faker) {
    $payload = '{
        "form_type": "'. $faker->randomElement(['Bootstrap', 'Material']) .'",
        "auto_submit": "'. $faker->boolean(50) .'",
        "submit_color": "'. $faker->hexcolor .'",
    }';

    return [
        'domain_id' => function () {
            return App\Domain::all()->random(1)->first()->id;
        },
        'name' => $faker->catchPhrase,
        'method' => $faker->randomElement(['get','post']),
        'controller' => 'LandingPage\LandingPageController',
        'action' => 'show',
        'root_path' => $faker->url,
        'path' => $faker->url,
        'type' => $faker->randomElement(['Web Site','Landing','Submit','Call']),
        'template' => 'landing.page',
        'payload' => $payload
    ];
});
