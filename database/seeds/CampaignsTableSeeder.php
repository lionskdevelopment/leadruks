<?php

use Illuminate\Database\Seeder;

class CampaignsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        factory(App\Campaign::class)->create([
            'client_id' => function () {
                return App\Client::where('name', 'Casualty Pro Restoration')->first()->id;
            },
            'name' => 'Miami - Water Damage Free Inspection'
        ]);
        factory(App\Campaign::class)->create([
            'client_id' => function () {
                return App\Client::where('name', 'Casualty Pro Restoration')->first()->id;
            },
            'name' => 'Broward - Water Damage Free Inspection'
        ]);

        if (App::environment('local')) {
            factory(App\Campaign::class)->create([
                'client_id' => function () {
                    return App\Client::where('name', 'Leadruks')->first()->id;
                },
                'name' => 'Miami - Free Inspection'
            ]);
            factory(App\Campaign::class)->create([
                'client_id' => function () {
                    return App\Client::where('name', 'Leadruks')->first()->id;
                },
                'name' => 'Broward - Free Inspection'
            ]);
//            factory(App\Campaign::class, 50)->create();
        }
    }
}
