<?php

use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // // floodrestorationmiami.com
        $domain_id = App\Domain::where('name', 'floodrestorationmiami.com')->first()->id;
        // Default - Landing Page
        factory(App\Page::class)->create([
            'domain_id' => function () {
                return App\Domain::where('name', 'floodrestorationmiami.com')->first()->id;
            },
            'controller' => 'LandingPage\LandingPageController',
            'action' => 'show',
            'name' => 'Default',
            'method' => 'get',
            'root_path' => '/',
            'path' => '/',
            'type' => 'Landing',
            'template' => 'casualty-pro-restoration/landing-page-1-template',
            'domain_default' => 1
        ]);
        // Default - Submit
        factory(App\Page::class)->create([
            'domain_id' => function () {
                return App\Domain::where('name', 'floodrestorationmiami.com')->first()->id;
            },
            'parent_page_id' => function () {
                return App\Page::where('name', 'Default')->first()->id;
            },
            'method' => 'post',
            'controller' => 'Lead\LeadController',
            'action' => 'store',
            'name' => 'Default Submit',
            'root_path' => '/',
            'path' => '/Submit',
            'type' => 'Submit',
            'template' => 'casualty-pro-restoration/landing-page-1-submit'
        ]);
        // Default - Call
        factory(App\Page::class)->create([
            'domain_id' => function () {
                return App\Domain::where('name', 'floodrestorationmiami.com')->first()->id;
            },
            'parent_page_id' => function () {
                return App\Page::where('name', 'Default')->first()->id;
            },
            'method' => 'get',
            'name' => 'Default Call',
            'root_path' => '/',
            'path' => '/Call',
            'type' => 'Call',
            'template' => 'casualty-pro-restoration/landing-page-1-call'
        ]);
        // Miami - Landing Page
        factory(App\Page::class)->create([
            'domain_id' => function () {
                return App\Domain::where('name', 'floodrestorationmiami.com')->first()->id;
            },
            'controller' => 'LandingPage\LandingPageController',
            'action' => 'show',
            'name' => 'Water Damage Free Inspection',
            'method' => 'get',
            'root_path' => '/Free-Inspection/Water-Damage',
            'path' => '/Free-Inspection/Water-Damage',
            'type' => 'Landing',
            'template' => 'casualty-pro-restoration/landing-page-1-template'
        ]);
        // Miami - Submit
        factory(App\Page::class)->create([
            'domain_id' => function () {
                return App\Domain::where('name', 'floodrestorationmiami.com')->first()->id;
            },
            'parent_page_id' => function () use ($domain_id) {
                return App\Page::where('domain_id', $domain_id)->where('path', '/Free-Inspection/Water-Damage')->first()->id;
            },
            'method' => 'post',
            'controller' => 'Lead\LeadController',
            'action' => 'store',
            'name' => 'Water Damage Free Inspection',
            'root_path' => '/Free-Inspection/Water-Damage',
            'path' => '/Free-Inspection/Water-Damage/Submit',
            'type' => 'Submit',
            'template' => 'casualty-pro-restoration/landing-page-1-submit'
        ]);
        // Miami - Call
        factory(App\Page::class)->create([
            'domain_id' => function () {
                return App\Domain::where('name', 'floodrestorationmiami.com')->first()->id;
            },
            'parent_page_id' => function () use($domain_id) {
                return App\Page::where('domain_id', $domain_id)->where('path', '/Free-Inspection/Water-Damage')->first()->id;
            },
            'method' => 'get',
            'name' => 'Water Damage Free Inspection',
            'root_path' => '/Free-Inspection/Water-Damage',
            'path' => '/Free-Inspection/Water-Damage/Call',
            'type' => 'Call',
            'template' => 'casualty-pro-restoration/landing-page-1-call'
        ]);

        // // floodrestorationbroward.com
        $domain_id = App\Domain::where('name', 'floodrestorationbroward.com')->first()->id;
        // Default - Landing Page
        factory(App\Page::class)->create([
            'domain_id' => function () {
                return App\Domain::where('name', 'floodrestorationbroward.com')->first()->id;
            },
            'controller' => 'LandingPage\LandingPageController',
            'action' => 'show',
            'name' => 'Default',
            'method' => 'get',
            'root_path' => '/',
            'path' => '/',
            'type' => 'Landing',
            'template' => 'casualty-pro-restoration/landing-page-1-template',
            'domain_default' => 1
        ]);
        // Default - Submit
        factory(App\Page::class)->create([
            'domain_id' => function () {
                return App\Domain::where('name', 'floodrestorationbroward.com')->first()->id;
            },
            'parent_page_id' => function () use($domain_id) {
                return App\Page::where('domain_id', $domain_id)->where('path', '/')->first()->id;
            },
            'method' => 'post',
            'controller' => 'Lead\LeadController',
            'action' => 'store',
            'name' => 'Default Submit',
            'root_path' => '/',
            'path' => '/Submit',
            'type' => 'Submit',
            'template' => 'casualty-pro-restoration/landing-page-1-submit'
        ]);
        // Default - Call
        factory(App\Page::class)->create([
            'domain_id' => function () {
                return App\Domain::where('name', 'floodrestorationbroward.com')->first()->id;
            },
            'parent_page_id' => function () use($domain_id) {
                return App\Page::where('domain_id', $domain_id)->where('path', '/')->first()->id;
            },
            'method' => 'get',
            'name' => 'Default Call',
            'root_path' => '/',
            'path' => '/Call',
            'type' => 'Call',
            'template' => 'casualty-pro-restoration/landing-page-1-call'
        ]);
        // Broward - Landing Page
        factory(App\Page::class)->create([
            'domain_id' => function () {
                return App\Domain::where('name', 'floodrestorationbroward.com')->first()->id;
            },
            'controller' => 'LandingPage\LandingPageController',
            'action' => 'show',
            'name' => 'Water Damage Free Inspection',
            'method' => 'get',
            'root_path' => '/Free-Inspection/Water-Damage',
            'path' => '/Free-Inspection/Water-Damage',
            'type' => 'Landing',
            'template' => 'casualty-pro-restoration/landing-page-1-template'
        ]);
        // Broward - Submit
        factory(App\Page::class)->create([
            'domain_id' => function () {
                return App\Domain::where('name', 'floodrestorationbroward.com')->first()->id;
            },
            'parent_page_id' => function () use($domain_id) {
                return App\Page::where('domain_id', $domain_id)->where('path', '/Free-Inspection/Water-Damage')->first()->id;
            },
            'method' => 'post',
            'controller' => 'Lead\LeadController',
            'action' => 'store',
            'name' => 'Water Damage Free Inspection',
            'root_path' => '/Free-Inspection/Water-Damage',
            'path' => '/Free-Inspection/Water-Damage/Submit',
            'type' => 'Submit',
            'template' => 'casualty-pro-restoration/landing-page-1-submit'
        ]);
        // Broward - Call
        factory(App\Page::class)->create([
            'domain_id' => function () {
                return App\Domain::where('name', 'floodrestorationbroward.com')->first()->id;
            },
            'parent_page_id' => function () use($domain_id) {
                return App\Page::where('domain_id', $domain_id)->where('path', '/Free-Inspection/Water-Damage')->first()->id;
            },
            'method' => 'get',
            'name' => 'Water Damage Free Inspection',
            'root_path' => '/Free-Inspection/Water-Damage',
            'path' => '/Free-Inspection/Water-Damage/Call',
            'type' => 'Call',
            'template' => 'casualty-pro-restoration/landing-page-1-call'
        ]);


        if (App::environment('local')) {
            // // LEADRUKS.DEV

            // Default - Landing Page
            factory(App\Page::class)->create([
                'domain_id' => function () {
                    return App\Domain::where('name', 'leadruks.dev')->first()->id;
                },
                'controller' => 'LandingPage\LandingPageController',
                'action' => 'show',
                'name' => 'Default',
                'method' => 'get',
                'root_path' => '/',
                'path' => '/',
                'type' => 'Landing',
                'template' => 'casualty-pro-restoration/landing-page-1-template',
                'domain_default' => 1
            ]);
            // Default - Submit
            factory(App\Page::class)->create([
                'domain_id' => function () {
                    return App\Domain::where('name', 'leadruks.dev')->first()->id;
                },
                'parent_page_id' => function () {
                    return App\Page::where('name', 'Default')->first()->id;
                },
                'method' => 'post',
                'controller' => 'Lead\LeadController',
                'action' => 'store',
                'name' => 'Default Submit',
                'root_path' => '/',
                'path' => '/Submit',
                'type' => 'Submit',
                'template' => 'casualty-pro-restoration/landing-page-1-submit'
            ]);
            // Default - Call
            factory(App\Page::class)->create([
                'domain_id' => function () {
                    return App\Domain::where('name', 'leadruks.dev')->first()->id;
                },
                'parent_page_id' => function () {
                    return App\Page::where('name', 'Default')->first()->id;
                },
                'method' => 'get',
                'name' => 'Default Call',
                'root_path' => '/',
                'path' => '/Call',
                'type' => 'Call',
                'template' => 'casualty-pro-restoration/landing-page-1-call'
            ]);

            // Miami - Landing Page
            factory(App\Page::class)->create([
                'domain_id' => function () {
                    return App\Domain::where('name', 'leadruks.dev')->first()->id;
                },
                'controller' => 'LandingPage\LandingPageController',
                'action' => 'show',
                'name' => 'Water Damage Free Inspection',
                'method' => 'get',
                'root_path' => '/Miami/Free-Inspection/Water-Damage',
                'path' => '/Miami/Free-Inspection/Water-Damage',
                'type' => 'Landing',
                'template' => 'casualty-pro-restoration/landing-page-1-template'
            ]);
            // Miami - Submit
            factory(App\Page::class)->create([
                'domain_id' => function () {
                    return App\Domain::where('name', 'leadruks.dev')->first()->id;
                },
                'parent_page_id' => function () {
                    return App\Page::where('name', 'Water Damage Free Inspection')->first()->id;
                },
                'method' => 'post',
                'controller' => 'Lead\LeadController',
                'action' => 'store',
                'name' => 'Water Damage Free Inspection',
                'root_path' => '/Miami/Free-Inspection/Water-Damage',
                'path' => '/Miami/Free-Inspection/Water-Damage/Submit',
                'type' => 'Submit',
                'template' => 'casualty-pro-restoration/landing-page-1-submit'
            ]);
            // Miami - Call
            factory(App\Page::class)->create([
                'domain_id' => function () {
                    return App\Domain::where('name', 'leadruks.dev')->first()->id;
                },
                'parent_page_id' => function () {
                    return App\Page::where('name', 'Water Damage Free Inspection')->first()->id;
                },
                'method' => 'get',
                'name' => 'Water Damage Free Inspection',
                'root_path' => '/Miami/Free-Inspection/Water-Damage',
                'path' => '/Miami/Free-Inspection/Water-Damage/Call',
                'type' => 'Call',
                'template' => 'casualty-pro-restoration/landing-page-1-call'
            ]);

            // Broward - Landing Page
            factory(App\Page::class)->create([
                'domain_id' => function () {
                    return App\Domain::where('name', 'leadruks.dev')->first()->id;
                },
                'controller' => 'LandingPage\LandingPageController',
                'action' => 'show',
                'name' => 'Water Damage Free Inspection',
                'method' => 'get',
                'root_path' => '/Broward/Free-Inspection/Water-Damage',
                'path' => '/Broward/Free-Inspection/Water-Damage',
                'type' => 'Landing',
                'template' => 'casualty-pro-restoration/landing-page-1-template'
            ]);
            // Broward - Submit
            factory(App\Page::class)->create([
                'domain_id' => function () {
                    return App\Domain::where('name', 'leadruks.dev')->first()->id;
                },
                'parent_page_id' => function () {
                    return App\Page::where('name', 'Water Damage Free Inspection')->first()->id;
                },
                'method' => 'post',
                'controller' => 'Lead\LeadController',
                'action' => 'store',
                'name' => 'Water Damage Free Inspection',
                'root_path' => '/Broward/Free-Inspection/Water-Damage',
                'path' => '/Broward/Free-Inspection/Water-Damage/Submit',
                'type' => 'Submit',
                'template' => 'casualty-pro-restoration/landing-page-1-submit'
            ]);
            // Broward - Call
            factory(App\Page::class)->create([
                'domain_id' => function () {
                    return App\Domain::where('name', 'leadruks.dev')->first()->id;
                },
                'parent_page_id' => function () {
                    return App\Page::where('name', 'Water Damage Free Inspection')->first()->id;
                },
                'method' => 'get',
                'name' => 'Water Damage Free Inspection',
                'root_path' => '/Broward/Free-Inspection/Water-Damage',
                'path' => '/Broward/Free-Inspection/Water-Damage/Call',
                'type' => 'Call',
                'template' => 'casualty-pro-restoration/landing-page-1-call'
            ]);

//            factory(App\Page::class, 10)->create();
        }
    }
}
