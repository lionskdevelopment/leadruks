<?php

use Illuminate\Database\Seeder;

class DomainsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Domain::class)->create([
            'client_id' => function () {
                return App\Client::where('name', 'Casualty Pro Restoration')->first()->id;
            },
            'name' => 'floodrestorationmiami.com',
            'google_tag_manager_id' => 'GTM-5SZQPRJ',
            'google_analytics_tracking_id' => 'UA-106403465-1',
        ]);
        factory(App\Domain::class)->create([
            'client_id' => function () {
                return App\Client::where('name', 'Casualty Pro Restoration')->first()->id;
            },
            'name' => 'floodrestorationbroward.com',
            'google_tag_manager_id' => 'GTM-TPRD7G2',
            'google_analytics_tracking_id' => 'UA-106403465-2',

        ]);

        if (App::environment('local')) {
            // Domain Fakery
            factory(App\Domain::class)->create([
                'client_id' => function () {
                    return App\Client::where('name', 'Leadruks')->first()->id;
                },
                'name' => 'leadruks.dev',
                'google_tag_manager_id' => 'GTM-5SZQPRJ',
                'google_analytics_tracking_id' => 'UA-106403465-1',
            ]);
//            factory(App\Domain::class, 10)->create();
        }



    }
}
