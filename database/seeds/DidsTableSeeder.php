<?php

use Illuminate\Database\Seeder;

class DidsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // floodrestorationmiami.com
        $domain_id = App\Domain::where('name', 'floodrestorationmiami.com')->first()->id;
        // default did
        factory(App\Did::class)->create([
            'page_id' => function () use ($domain_id) {
                return App\Page::where('domain_id', $domain_id)->where('path', '/')->first()->id;
            },
            'number' => '786-508-1100',
            'vendor' => '3NG Network',
            'placement' => 1
        ]);
        // /Free-Inspection/Water-Damage
        factory(App\Did::class)->create([
            'page_id' => function () use ($domain_id) {
                return App\Page::where('domain_id', $domain_id)->where('path', '/Free-Inspection/Water-Damage')->first()->id;
            },
            'number' => '786-565-1500',
            'vendor' => '3NG Network',
            'placement' => 1
        ]);
        factory(App\Did::class)->create([
            'page_id' => null,
            'number' => '786-416-0022',
            'vendor' => '3NG Network',
            'placement' => 1
        ]);

        // floodrestorationbroward.com
        $domain_id = App\Domain::where('name', 'floodrestorationbroward.com')->first()->id;
        // default did
        factory(App\Did::class)->create([
            'page_id' => function () use ($domain_id) {
                return App\Page::where('domain_id', $domain_id)->where('path', '/')->first()->id;
            },
            'number' => '954-947-3993',
            'vendor' => '3NG Network',
            'placement' => 1
        ]);
        // /Free-Inspection/Water-Damage
        factory(App\Did::class)->create([
            'page_id' => function () use ($domain_id) {
                return App\Page::where('domain_id', $domain_id)->where('path', '/Free-Inspection/Water-Damage')->first()->id;
            },
            'number' => '954-687-0909',
            'vendor' => '3NG Network',
            'placement' => 1
        ]);

        if (App::environment('local')) {
            // leadruks.dev seeding
            $domain_id = App\Domain::where('name', 'leadruks.dev')->first()->id;
            // Default
            factory(App\Did::class)->create([
                'page_id' => function () use ($domain_id) {
                    return App\Page::where('domain_id', $domain_id)->where('path', '/')->first()->id;
                },
                'number' => '305-300-1111',
                'vendor' => '3NG Network',
                'placement' => 1
            ]);
            factory(App\Did::class)->create([
                'page_id' => function () use ($domain_id) {
                    return App\Page::where('domain_id', $domain_id)->where('path', '/Miami/Free-Inspection/Water-Damage')->first()->id;
                },
                'number' => '305-300-0001',
                'vendor' => '3NG Network',
                'placement' => 1
            ]);
            factory(App\Did::class)->create([
                'page_id' => function () use ($domain_id) {
                    return App\Page::where('path', '/Broward/Free-Inspection/Water-Damage')->first()->id;
                },
                'number' => '954-300-0001',
                'vendor' => '3NG Network',
                'placement' => 1
            ]);

//            factory(App\Did::class, 10)->create();
        }
    }
}
