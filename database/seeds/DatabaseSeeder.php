<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ClientsTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(CampaignsTableSeeder::class);
        $this->call(DomainsTableSeeder::class);
        $this->call(PagesTableSeeder::class);
        $this->call(FormsTableSeeder::class);
        $this->call(DidsTableSeeder::class);
        $this->call(LeadsTableSeeder::class);
    }
}
