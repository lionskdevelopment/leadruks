<?php

use Illuminate\Database\Seeder;


class ClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Client::class)->create([
            'name' => 'Casualty Pro Restoration',
            'google_adwords_id' => '180-547-3376'
        ]);

        if (App::environment('local')) {
            // Clients Fakery
            factory(App\Client::class)->create([
                'name' => 'Leadruks',
                'google_adwords_id' => '180-547-3376'
            ]);
//            factory(App\Client::class, 10)->create();
        }
    }
}
