<?php

use Illuminate\Database\Seeder;

class FormsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (App::environment('local')) {
            $domain_id = App\Domain::where('name', 'leadruks.dev')->first()->id;
            factory(App\Form::class)->create([
                'page_id' => function () use ($domain_id) {
                    return App\Page::where('domain_id', $domain_id)->where('path', '/')->first()->id;
                },
                'name' => 'Default Form',
                'type' => 'Landing Page',
                'submit' => '/Submit',
                'thank_you_template' => 'casualty-pro-restoration/landing-page-1-thank-you',
                'payload' => '{ "form_type": "Bootstrap" }'
            ]);
            factory(App\Form::class)->create([
                'page_id' => function () use ($domain_id) {
                    return App\Page::where('domain_id', $domain_id)->where('path', '/Miami/Free-Inspection/Water-Damage')->first()->id;
                },
                'name' => 'Miami - Free Inspection',
                'type' => 'Landing Page',
                'submit' => '/Miami/Free-Inspection/Water-Damage/Submit',
                'thank_you_template' => 'casualty-pro-restoration/landing-page-1-thank-you',
                'payload' => '{ "form_type": "Bootstrap" }'
            ]);
            factory(App\Form::class)->create([
                'page_id' => function () use ($domain_id) {
                    return App\Page::where('domain_id', $domain_id)->where('path', '/Broward/Free-Inspection/Water-Damage')->first()->id;
                },
                'name' => 'Broward - Free Inspection',
                'type' => 'Landing Page',
                'submit' => '/Broward/Free-Inspection/Water-Damage/Submit',
                'thank_you_template' => 'casualty-pro-restoration/landing-page-1-thank-you',
                'payload' => '{ "form_type": "Bootstrap" }'
            ]);

//            factory(App\Form::class, 10)->create();
        } else if (App::environment('production')) {
            $domain_id = App\Domain::where('name', 'floodrestorationmiami.com')->first()->id;
            factory(App\Form::class)->create([
                'page_id' => function () use ($domain_id) {
                    return App\Page::where('domain_id', $domain_id)->where('path', '/')->first()->id;
                },
                'name' => 'Default Form',
                'type' => 'Landing Page',
                'submit' => '/Submit',
                'thank_you_template' => 'casualty-pro-restoration/landing-page-1-thank-you',
                'payload' => '{ "form_type": "Bootstrap" }'
            ]);
            factory(App\Form::class)->create([
                'page_id' => function () use ($domain_id) {
                    return App\Page::where('domain_id', $domain_id)->where('path', '/Free-Inspection/Water-Damage')->first()->id;
                },
                'name' => 'Free Same Day Inspection',
                'type' => 'Landing Page',
                'submit' => '/Free-Inspection/Water-Damage/Submit',
                'thank_you_template' => 'casualty-pro-restoration/landing-page-1-thank-you',
                'payload' => '{ "form_type": "Bootstrap" }'
            ]);

            $domain_id = App\Domain::where('name', 'floodrestorationbroward.com')->first()->id;
            factory(App\Form::class)->create([
                'page_id' => function () use ($domain_id) {
                    return App\Page::where('domain_id', $domain_id)->where('path', '/')->first()->id;
                },
                'name' => 'Default Form',
                'type' => 'Landing Page',
                'submit' => '/Submit',
                'thank_you_template' => 'casualty-pro-restoration/landing-page-1-thank-you',
                'payload' => '{ "form_type": "Bootstrap" }'
            ]);
            factory(App\Form::class)->create([
                'page_id' => function () use ($domain_id) {
                    return App\Page::where('domain_id', $domain_id)->where('path', '/Free-Inspection/Water-Damage')->first()->id;
                },
                'name' => 'Free Same Day Inspection',
                'type' => 'Landing Page',
                'submit' => '/Free-Inspection/Water-Damage/Submit',
                'thank_you_template' => 'casualty-pro-restoration/landing-page-1-thank-you',
                'payload' => '{ "form_type": "Bootstrap" }'
            ]);
        }
    }
}
